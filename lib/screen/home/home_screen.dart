import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  String title;

  HomeScreen(this.title);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("HOME SCREEN"),
      ),
      body: ListView(
        children: <Widget>[Text(title)],
      ),
    );
  }
}
