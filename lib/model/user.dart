class User {
  String name;
  String email;
  String token;

  User({this.name, this.email, this.token});

  factory User.fromMap(dynamic map) {
    var name = map['user']['name'] == null ? null : map['user']['name'];
    var email = map['user']['email'] == null ? null : map['user']['email'];
    var token = map['access_token'] == null ? null : map['access_token'];
    return User(name: name, email: email, token: token);
  }

  @override
  String toString() {
    return 'User{name: $name, email: $email, token: $token}';
  }


}
